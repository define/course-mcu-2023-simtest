/*
 * @Author: CaoWangrenbo cao.wangrenbo@yandex.com
 * @Date: 2023-02-20 23:40:23
 * @LastEditors: CaoWangrenbo cao.wangrenbo@yandex.com
 * @LastEditTime: 2023-02-21 00:14:54
 * @FilePath: \Source\src\main.c
 * @Description: Q2 数码管+按键
 * 
 * 
 */

#include "REG52.H"

typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long uint32_t;

typedef signed char int8_t;
typedef signed int int16_t;
typedef signed long int32_t;

#define EXT0_VECTOR 0  /* 0x03 external interrupt 0 */
#define TIM0_VECTOR 1  /* 0x0b timer 0 */
#define EXT1_VECTOR 2  /* 0x13 external interrupt 1 */
#define TIM1_VECTOR 3  /* 0x1b timer 1 */
#define UART0_VECTOR 4 /* 0x23 serial port 0 */

void Delay10ms();
void Key_Scan();
void LED_Show_Num_U8(uint8_t num);

sbit KEY = P1 ^ 0;

uint8_t g_num = 0;  //按键次数
uint8_t g_led_num[10] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90};   //数码管真值表

void main()
{

    while (1)
    {
        Key_Scan(); //按键扫描函数
        LED_Show_Num_U8(g_num); //显示数字
    }
}

/**
 * @brief 软件延时 10ms @12.0000MHz
 * 
 */
void Delay10ms()
{
	unsigned char i, j;

	i = 20;
	j = 113;
	do
	{
		while (--j);
	} while (--i);
}

/**
 * @brief 按键扫描函数
 * @details 其实 次数自增功能还可以使用计数器实现，但是为了简单，直接轮询后累加即可
 */
void Key_Scan()
{
    if(0 == KEY)
    {
        Delay10ms();    //延时消抖
        if(0 == KEY)
        {
            if(10 == ++g_num)   //按下次数自增，超过9次（第10次）清零
            {
                g_num = 0;
            }
        }
        while(!KEY);    //等待松开按键
    }
}

/**
 * @brief 数码管显示函数
 * 
 * @param num 显示数值
 */
void LED_Show_Num_U8(uint8_t num)
{
    P2 = g_led_num[num];
}