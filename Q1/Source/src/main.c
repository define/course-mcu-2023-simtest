/*
 * @Author: CaoWangrenbo cao.wangrenbo@yandex.com
 * @Date: 2023-02-20 22:22:21
 * @LastEditors: CaoWangrenbo cao.wangrenbo@yandex.com
 * @LastEditTime: 2023-02-21 08:57:43
 * @FilePath: \Source\src\main.c
 * @Description: Q1 按键及定时器例程
 * 
 * 
 */

#include "REG52.H"

typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long uint32_t;

typedef signed char int8_t;
typedef signed int int16_t;
typedef signed long int32_t;

#define EXT0_VECTOR 0  /* 0x03 external interrupt 0 */
#define TIM0_VECTOR 1  /* 0x0b timer 0 */
#define EXT1_VECTOR 2  /* 0x13 external interrupt 1 */
#define TIM1_VECTOR 3  /* 0x1b timer 1 */
#define UART0_VECTOR 4 /* 0x23 serial port 0 */

uint16_t g_mod = 1;  //闪烁模式标志位，1为0.5闪烁一次，4为2秒闪烁一次

// LED pin define
sbit LED = P1 ^ 0;
sbit KEY = P1 ^ 1;

void Delay10ms();
uint16_t Key_Scan();


void main()
{
    /*****************************************************************************************
        晶振频率选12MHz，故机器周期 T_机 = 12/12 = 1us
        选中断周期 1ms, 故定时器计数1000次后溢出，设定16位计时器模式，故定时器初值 65536-1000 = 64536
    *****************************************************************************************/
    // set T0 1ms
    TMOD = 0x01;    //16位寄存器
    TH0 = 0xFC;     //初值0xFC18 即64536
    TL0 = 0x18;

    // enable interrupt
    EA = 1;     //开总中断
    ET0 = 1;    //开定时器 T0 中断

    // launch T0
    TR0 = 1;    //启动定时器 T0

    while (1)
    {
        g_mod = Key_Scan(); //扫描带有阻塞延时，所以只能在while(1)内更新
    }
}

/**
 * @brief 定时器0中断回调函数
 * 
 */
void TIM0_Handler() interrupt TIM0_VECTOR
{
    static uint16_t count;

    TH0 = 0xFC; //重新加载初值
    TL0 = 0x18;

    if (++count >= (500 * g_mod))
    {
        count = 0;
        LED = !LED;
    }
}

/**
 * @brief 延时10ms @12.000MHz
 * 
 */
void Delay10ms()
{
	uint8_t i, j;

	i = 20;
	j = 113;
	do
	{
		while (--j);
	} while (--i);
}

/**
 * @brief 按键扫描
 * 
 * @return uint8_t 按下为4，松开为1
 */
uint16_t Key_Scan()
{
    if(0 == KEY)    //按下为0
    {
        Delay10ms();
        if(0 == KEY)
        {
            return 1;
        }
    }

    return 4;
}