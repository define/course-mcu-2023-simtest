<!--
 * @Author: CaoWangrenbo cao.wangrenbo@yandex.com
 * @Date: 2023-02-21 08:46:41
 * @LastEditors: CaoWangrenbo cao.wangrenbo@yandex.com
 * @LastEditTime: 2023-02-21 08:47:45
 * @FilePath: \undefinedc:\Users\ForgotDoge\Desktop\编程题\Readme.md
 * @Description: 
 * 
 * 
-->
# 模拟题解

## 1.题目及运行效果：

![题目](https://gitea.com/define/course-mcu-2023-simtest/raw/branch/main/pic/q.png)
![Q1运行效果](https://gitea.com/define/course-mcu-2023-simtest/raw/branch/main/pic/Q1.gif)
![Q2运行效果](https://gitea.com/define/course-mcu-2023-simtest/raw/branch/main/pic/Q2.gif)

## 2.文件夹结构：
```├─pic
├─Q1	第一题题解
│  ├─Simulation 仿真文件
│  └─Source	源代码
└─Q2	第二题题解
    ├─Simulation仿真文件
    └─Source	源代码
```

## 3.说明：
> 晚上无聊写的，代码比较糟糕，请见谅

* 代码是在 VSCode 里用 EIDE 编的，比较懒所以没整 Keil 工程，需要编译的话自行建立 Keil C51 工程即可。
* 仿真文件是 Proteus 8.9 编辑的，所以只能保存为 `.pdsprj` 格式，打不开的话我也没办法2333
* 代码全部运行正常，符合要求

```
祝同志们考试顺利！
⣿⣿⣿⣿⣿⣿⢟⣡⣴⣶⣶⣦⣌⡛⠟⣋⣩⣬⣭⣭⡛⢿⣿⣿⣿⣿
⣿⣿⣿⣿⠋⢰⣿⣿⠿⣛⣛⣙⣛⠻⢆⢻⣿⠿⠿⠿⣿⡄⠻⣿⣿⣿
⣿⣿⣿⠃⢠⣿⣿⣶⣿⣿⡿⠿⢟⣛⣒⠐⠲⣶⡶⠿⠶⠶⠦⠄⠙⢿
⣿⠋⣠⠄⣿⣿⣿⠟⡛⢅⣠⡵⡐⠲⣶⣶⣥⡠⣤⣵⠆⠄⠰⣦⣤⡀
⠇⣰⣿⣼⣿⣿⣧⣤⡸⢿⣿⡀⠂⠁⣸⣿⣿⣿⣿⣇⠄⠈⢀⣿⣿⠿
⣰⣿⣿⣿⣿⣿⣿⣿⣷⣤⣈⣙⠶⢾⠭⢉⣁⣴⢯⣭⣵⣶⠾⠓⢀⣴
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣉⣤⣴⣾⣿⣿⣦⣄⣤⣤⣄⠄⢿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⠿⠿⠿⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣇⠈⢿
⣿⣿⣿⣿⣿⣿⡟⣰⣞⣛⡒⢒⠤⠦⢬⣉⣉⣉⣉⣉⣉⣉⡥⠴⠂⢸
⠻⣿⣿⣿⣿⣏⠻⢌⣉⣉⣩⣉⡛⣛⠒⠶⠶⠶⠶⠶⠶⠶⠶⠂⣸⣿
⣥⣈⠙⡻⠿⠿⣷⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⣿⠿⠛⢉⣠⣶⣶⣿⣿
⣿⣿⣿⣶⣬⣅⣒⣒⡂⠈⠭⠭⠭⠭⠭⢉⣁⣄⡀⢾⣿⣿⣿⣿⣿⣿
```